import os
from .extensions import db, migrate
from flask import Blueprint, Flask
from flask_bcrypt import Bcrypt
from flask_restx import Api
from .config import app_config
from .fund.resources import ns as fund_ns
from .portfolio.resources import ns as portfolio_ns
from .strategy.resources import ns as strategy_ns

# Extensions

flask_bcrypt = Bcrypt()
blueprint = Blueprint("preview", __name__)


# Namespaces
api = Api(
    blueprint,
    title="Portfolio Tool API with Flask",
    version="preview",
    description="Sprint 1 api design",
)
api.add_namespace(portfolio_ns)
api.add_namespace(strategy_ns)
api.add_namespace(fund_ns)


def create_app(config_name="development"):
    app = Flask(__name__)

    # Configuration
    app.config.from_object(app_config[config_name])
    # app.config.from_pyfile("config.py", silent=True)

    # Database
    db.init_app(app)

    # Migration
    migrate.init_app(app, db)

    # Crypto
    flask_bcrypt.init_app(app)

    # Versioning
    app.register_blueprint(blueprint, url_prefix="/v0")

    # Ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # Alive
    @app.route("/ping")
    def hello():
        return "pong"

    return app
