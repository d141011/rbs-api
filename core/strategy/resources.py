from flask_restx import Resource, Namespace, fields
from .models import Strategy
from ..portfolio.models import Portfolio

ns = Namespace("strategies", description="投資應用策略資料")

# strategy in the portfolio
strategy = ns.model(
    "Strategy",
    {
        "id": fields.Integer(required=True, description="Unique identifier"),
        "name": fields.String(readOnly=True, description="Strategy name"),
        "name_cht": fields.String(readOnly=True, description="Strategy name"),
    },
)

#  Investment condition
filter = ns.model(
    "StrategyCondition",
    {
        "strategy_id": fields.Integer(required=True, description="Strategy identifier"),
        "investment_amount": fields.Float(
            required=True, description="Total investment amount"
        ),
        "risk_level": fields.String(
            required=True, description="The risk tolerance level"
        ),
        "start_time": fields.Date(required=True, description="Start time"),
        "end_time": fields.Date(required=True, description="End time"),
    },
)

target = ns.model(
    "StrategyTarget",
    {"target_value": fields.Float(readOnly=True, description="Available value")},
)


@ns.route("/")
class StrategyListResource(Resource):
    """Shows a list of all strategies, and lets you POST to add new strategies"""

    @ns.doc("List all the strategies in the database")
    @ns.marshal_list_with(strategy)
    def get(self):
        """List all strategies"""
        return Strategy.query.all()


@ns.route("/targets")
@ns.response(404, "targets not found")
class TargetResource(Resource):
    @ns.doc("Get available targets from the condition")
    @ns.expect(filter)
    @ns.marshal_list_with(target)
    def post(self):
        """Get available targets from the condition"""
        strategy = Strategy.query.get(ns.payload["strategy_id"])
        return (
            strategy.portfolios.filter(
                Portfolio.lower_bound <= ns.payload["investment_amount"],
                Portfolio.upper_bound >= ns.payload["investment_amount"],
                Portfolio.record_dt <= ns.payload["start_time"],
                risk_level=ns.payload["risk_level"],
            )
            .order_by(Portfolio.target_value.desc())
            .distinct()
            .all()
        )
