from .. import db


class Strategy(db.Model):

    __tablename__ = "strategy"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    name_cht = db.Column(db.String(255), nullable=False)
    portfolios = db.relationship("Portfolio", backref="strategy", lazy=True)

    def __repr__(self):
        return "<Strategy %r>" % self.name
