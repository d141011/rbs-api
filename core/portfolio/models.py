from .. import db


class Portfolio(db.Model):

    __tablename__ = "portfolio"

    _id = db.Column(db.Integer, primary_key=True)
    portfolio_id = db.Column(db.Integer, nullable=False)
    robin_fund_code_xyz = db.Column(
        db.String(20), db.ForeignKey("fund.robin_fund_code"), nullable=False
    )
    weight = db.Column(db.Float, nullable=False)
    risk_level = db.Column(db.String(20), nullable=False)
    lower_bound = db.Column(db.Float, nullable=False)
    upper_bound = db.Column(db.Float, nullable=False)
    strategy_id = db.Column(db.Integer, db.ForeignKey("strategy.id"), nullable=False)
    target_value = db.Column(db.Float, nullable=False)
    start_time = db.Column(db.Date, nullable=False)
    end_time = db.Column(db.Date, nullable=False)

    def __repr__(self):
        return "<Portfolio %r>" % self.portfolio_id


class Rebalance(db.Model):

    __tablename__ = "rebalance"

    id = db.Column(db.Integer, primary_key=True)
    robin_fund_code = db.Column(
        db.String(20), db.ForeignKey("fund.robin_fund_code"), nullable=False
    )
    portfolio_id = db.Column(
        db.Integer, db.ForeignKey("portfolio.portfolio_id"), nullable=False
    )
    investment_amount = db.Column(db.Float, nullable=False)
    current_amount = db.Column(db.Float, nullable=False)
    current_amount_ratio = db.Column(db.Float, nullable=False)
    accumulated_return = db.Column(db.Float, nullable=False)
    accumulated_total_return = db.Column(db.Float, nullable=False)
    tx_date = db.Column(db.Date, nullable=False)
    tx_interval = db.Column(db.String(4), nullable=False)

    def __repr__(self):
        return "<Rebalance at %r>" % self.tx_date
