from flask_restx import Resource, fields, Namespace
from .models import Rebalance, Portfolio
from ..fund.resources import fund


ns = Namespace("portfolios", description="投組配置／再平衡服務")

# Portfolio suggested
portfolio = ns.model(
    "PortfolioSuggestion",
    {
        "id": fields.Integer(readOnly=True, description="Unique identifier"),
        "fund": fields.Nested(fund),
        "weight": fields.Float(
            readOnly=True, description="Fund's weight in the portfolio"
        ),
    },
)

# Input Payload
condition = ns.model(
    "PortfolioCondition",
    {
        "start_date": fields.Date(required=True, description="Start time"),
        "end_date": fields.Date(required=True, description="End time"),
        "risk_level": fields.String(
            required=True, description="Risk return level (RR)"
        ),
        "investment_amount": fields.Float(
            required=True, description="Total investment amount"
        ),
        "strategy_id": fields.Integer(
            required=True, description="The algorithm for the type selected"
        ),
        "target_value": fields.Float(
            required=True,
            description="The ROI (return on investment) / target volatility depending on the investment strategy",
        ),
        "tx_currency": fields.String(
            required=True, description="The currency related to the input"
        ),
    },
)

# Rebalance filter
filter = ns.model(
    "RebalanceFilter",
    {
        "start_date": fields.Date(required=True, description="Start time"),
        "end_date": fields.Date(required=True, description="End time"),
        "portfolio_id": fields.Integer(
            required=True, description="Portfolio Identifier"
        ),
        "tx_interval": fields.String(
            required=True,
            default="1m",
            description="The interval of a tick, ex. 1d, 1m, 3m",
        ),
        "tx_currency": fields.String(
            required=True, description="The currency related to the input"
        ),
    },
)

# Rebalance record
record = ns.model(
    "PortfolioRebalance",
    {
        "portfolio_id": fields.Integer(
            readOnly=True, description="Portfolio Group Identifier"
        ),
        "fund": fields.Ｎested(fund),
        "current_amount": fields.Float(readOnly=True, description="Fund amount"),
        "current_amount_ratio": fields.Float(
            readOnly=True, description="The ratio of the fund amount"
        ),
        "accumulated_return": fields.Float(
            readOnly=True, description="Accumulated Return"
        ),
        "accumulated_total_return": fields.Float(
            readOnly=True, description="Accumulated Total Return"
        ),
        "tx_date": fields.Date(readOnly=True, description="Transaction date"),
    },
)

"""
# Financial measurement
measurement = ns.model(
    "Measurement",
    {

    },
)
"""


@ns.route("/")
class PortfolioResource(Resource):
    @ns.doc("Submit a condition and return the suggestion")
    @ns.expect(condition)
    @ns.marshal_list_with(portfolio)
    def post(self):
        """Submit a condition and return the suggestion"""
        portfolios = Portfolio.query.filter(
            Portfolio.risk_level == ns.payload["risk_level"],
            Portfolio.strategy_id == ns.payload["strategy_id"],
            # Portfolio.target_value==ns.payload["target_value"],
            # Portfolio.lower_bound < ns.payload["investment_amount"],
            # Portfolio.upper_bound > ns.payload["investment_amount"],
        ).all()
        return [
            {"id": item.portfolio_id, "fund": item.fund, "weight": item.weight}
            for item in portfolios
        ]


@ns.route("/rebalance")
class RebalanceResource(Resource):
    @ns.doc("Submit the filter and return records")
    @ns.expect(condition)
    @ns.marshal_list_with(record)
    def post(self):
        """Submit the filter and return records"""
        data = Rebalance(
            portfolio_id=ns.payload["portfolio_id"],
            tx_interval=ns.payload["tx_interval"],
        )
        return (
            Rebalance.query(data)
            .filter(
                Rebalance.tx_date >= ns.payload["start_date"],
                Rebalance.tx_date <= ns.payload["end_date"],
            )
            .all(),
            201,
        )


'''
@ns.route("/measurements/<int:id>")
class MeasurementResource(Resource):
    @ns.doc("Return a portfolio's measurements")
    @ns.marshal_list_with(measurement)
    def get(self, id):
        """Return a portfolio's measurements"""
        portfolio=Portfolio.query.get(id)
        return Measurement.filter()
'''
