from flask_restx import Resource, fields, Namespace
from .models import Fund

# API payload - Fund in the portfolio
ns = Namespace("funds", description="基金資料")
fund = ns.model(
    "Fund",
    {
        "robin_fund_code": fields.String(
            readOnly=True, description="Robin fund code (global identifier)"
        ),
        "fund_name": fields.String(readOnly=True, description="Fund name"),
        "fund_name_cht": fields.String(readOnly=True, description="Fund name"),
        "index_name": fields.String(readOnly=True, description="Index name"),
        "index_name_cht": fields.String(readOnly=True, description="Index name"),
        "investment_type": fields.String(readOnly=True, description="Fund type"),
        "min_purchase_price": fields.Float(
            readOnly=True, description="Minimum amount of investment"
        ),
        "original_currency": fields.String(
            required=True, description="Original currency"
        ),
        "investment_amount": fields.Float(
            required=True, description="Amount of investment"
        ),
        "total_pct": fields.String(
            readOnly=True, description="Percentage of the portfolio"
        ),
    },
)


@ns.route("/")
class FundListResource(Resource):
    """Shows a list of all funds, and lets you POST to add new funds"""

    @ns.doc("List all the funds in the database")
    @ns.marshal_list_with(fund)
    def get(self):
        """List all funds"""
        return Fund.query.all()


@ns.route("/<string:robin_fund_code>")
@ns.response(404, "Fund not found")
@ns.param("robin_fund_code", "The fund identifier")
class FundResource(Resource):
    """These funds offer different investing options"""

    @ns.doc("Get a fund by RFC")
    @ns.marshal_with(fund)
    def get(self, robin_fund_code):
        """Fetch a given resource"""
        fund = Fund.query.get(robin_fund_code)
        print("test")
        print(fund)
        return (
            fund
            if fund is not None
            else ns.abort(404, "Fund {} doesn't exist".format(id))
        )
