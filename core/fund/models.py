from .. import db


class Fund(db.Model):

    __tablename__ = "fund"

    robin_fund_code = db.Column(db.String(20), primary_key=True)
    fund_name = db.Column(db.String(255), nullable=False)
    fund_name_cht = db.Column(db.String(255), nullable=False)
    index_name = db.Column(db.String(255), nullable=False)
    index_name_cht = db.Column(db.String(255), nullable=False)
    risk_level = db.Column(db.String(20), nullable=False)
    min_purchase_price = db.Column(db.Integer, nullable=False)
    investment_increment = db.Column(db.Integer, nullable=False)
    investment_type = db.Column(db.String(20), nullable=False)
    record_dt = db.Column(db.Date, nullable=False)
    portfolios = db.relationship("Portfolio", backref="fund", lazy=True)
    dividends = db.relationship("Dividend", backref="fund", lazy=True)
    returns = db.relationship("Return", backref="fund", lazy=True)

    def __repr__(self):
        return "<Fund %r>" % self.robin_fund_code


class Dividend(db.Model):
    __tablename__ = "fund_rolling_dividend_rate"

    robin_fund_code = db.Column(
        db.String(20), db.ForeignKey("fund.robin_fund_code"), primary_key=True
    )
    return_value = db.Column(db.Float, nullable=False)
    period = db.Column(db.Integer, nullable=False, primary_key=True)
    start_time = db.Column(db.Date, nullable=False)
    record_dt = db.Column(db.Date, nullable=False)

    def __repr__(self):
        return "<Fund %r dividend %r for %r month(s)>" % (
            self.robin_fund_code,
            self.return_value,
            self.period,
        )


class Return(db.Model):
    __tablename__ = "fund_rolling_total_return"

    robin_fund_code = db.Column(
        db.String(20), db.ForeignKey("fund.robin_fund_code"), primary_key=True
    )
    return_value = db.Column(db.Float, nullable=False)
    period = db.Column(db.Integer, nullable=False, primary_key=True)
    start_time = db.Column(db.Date, nullable=False)
    record_dt = db.Column(db.Date, nullable=False)

    def __repr__(self):
        return "<Fund %r return %r for %r month(s)>" % (
            self.robin_fund_code,
            self.return_value,
            self.period,
        )
