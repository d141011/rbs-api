# 投資組合再平衡 #

Sprint 1 Api Design 

## Prototype ##

### UI ###
![UIflow](uiflow.jpg "介面圖")

### Flowchart ###
![Flowchart](flowchart.jpg "流程圖")

### EER Diagram ###
![EER Diagram](diagram.png "實體關係模型")

## How to set up ##

Install the libraries in ```requirements.txt``` by executing
```
make install
```
and set the environment variable ```FLASK_APP=core```.  

You are suggested to set up ```virtualenv```, a full guide can be found at [Documentation](https://flask.palletsprojects.com/en/1.1.x/installation/), there are a few intructions defined in ```make venv```.


Modify ```core/config.py``` to connect the database by the environment variable ```SQLALCHEMY_DATABASE_URI``` with your settings.

If a testing database is needed, you can use the migrate commands below to create schemas.
```
flask db init
flask db mirgrate -m "init"
flask db upgrade
```

Run the flask application, 
```
export FLASK_APP=core
flask run
```

You can test the connection by,
```
curl -XGET http://127.0.0.1:5000/ping
```

The swagger document can be opened by,
```
http://127.0.0.1:5000/v0
```

A yaml file will be generated in bitbucket download folder ```swagger.yaml``` when CI triggered. 

Notes: v0 is the preview version.


## Who do I talk to? ##

* itri458960@itri.org.tw / 羅中廷
