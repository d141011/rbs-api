all: install

bootstrap:
	echo "Installing Pip"
	sudo apt-get install python-pip
	echo "Installing virtualenv"
	sudo pip install virtualenv
	sudo pip install nose

venv: 	
	bootstrap
	virtualenv venv

install:
	echo "Installing packages from requirements.txt"
	pip install -r requirements.txt

run:
	flask run

db:
	flask db init
	flask db migrate -m "init"
	flask db upgrade

clean:
	rm *.pyc