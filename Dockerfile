FROM python:3.8-slim

WORKDIR /app
ADD . /app
RUN apt-get update && apt-get install make
RUN make install
ENV FLASK_APP=core
EXPOSE 5000
CMD ["flask","run","--host=0.0.0.0"]